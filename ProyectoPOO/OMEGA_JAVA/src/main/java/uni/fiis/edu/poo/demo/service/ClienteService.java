package uni.fiis.edu.poo.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uni.fiis.edu.poo.demo.bean.Cliente;
import uni.fiis.edu.poo.demo.dao.ClienteDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

@Service
public class ClienteService {

    @Autowired
    ClienteDAO clienteDAO;

    @Autowired
    private JdbcTemplate template;

    public ResponseEntity registrarCliente(Cliente cliente) throws Exception{
        Connection connection = template.getDataSource().getConnection();
        String sql1 = "SELECT COUNT(*) FROM CLIENTE WHERE COD_UNI = ?;";
        PreparedStatement pst1 = connection.prepareStatement(sql1);
        pst1.setString(1,cliente.getCod_uni());
        ResultSet rs = pst1.executeQuery();
        rs.next();
        int cant = rs.getInt(1);
        rs.close();
        pst1.close();
        connection.close();
        if (cant==0){
            clienteDAO.registrarCliente(cliente);
            return new ResponseEntity("Cliente registrado", HttpStatus.OK);
        }else {
            return new ResponseEntity("Cliente ya existente.",HttpStatus.OK);
        }
    }

    public void actualizarCliente(String cod_uni, String newpassword) throws Exception{
        clienteDAO.actualizarCliente(cod_uni,newpassword);
    }

    public void eliminarCliente(String cod_uni) throws Exception{
        clienteDAO.eliminarCliente(cod_uni);
    }

    public Cliente buscarCliente(String cod_uni) throws Exception{
        return clienteDAO.buscarCliente(cod_uni);
    }

    public List<Cliente> mostrarCliente() throws Exception{
        return clienteDAO.mostrarCliente();
    }
}
