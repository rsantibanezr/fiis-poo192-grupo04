package uni.fiis.edu.poo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import uni.fiis.edu.poo.demo.bean.Administrador;
import uni.fiis.edu.poo.demo.dao.AdministradorDAO;
import uni.fiis.edu.poo.demo.dao.AdministradorDAOimpl;
import uni.fiis.edu.poo.demo.request.AdministradorRequest;
import uni.fiis.edu.poo.demo.service.AdministradorService;

import java.util.List;

@RestController
public class AdministradorController {

    @Autowired
    AdministradorService administradorService;

    @PostMapping("/administrador/registrar")
    public ResponseEntity registrarAdministrador(@RequestParam String password,
                                                 @RequestParam String nombre,
                                                 @RequestParam String ap_paterno,
                                                 @RequestParam String ap_materno,
                                                 @RequestParam String dni,
                                                 @RequestParam String email,
                                                 @RequestParam int telefono) throws Exception {
        int id_admin = administradorService.cantidadAdministrador() + 1;
        Administrador administrador = new Administrador(id_admin,password,nombre,ap_paterno,ap_materno,dni,email,telefono);
        try {
            return administradorService.registrarAdministrador(administrador);
        } catch (Exception e){
            return new ResponseEntity("No se pudo registrar.",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/administrador/actualizar")
    public String actualizarAdministrador
            (@RequestParam String email,
             @RequestParam String newpassword) throws Exception{
        try {
            if (administradorService.buscarAdministrador(email) != null) {
                administradorService.actualizarAdministrador(email, newpassword);
                return "<h1>Usuario: "+ email +" ha sido actualizado.</h1>";
            }else{
                return "<h1>Usuario no existe.</h1>";
            }
        } catch (Exception e){
            return "<h1>No se puedo actualizar usuario.</h1>";
        }
    }

    @GetMapping("/administrador/eliminar")
    public ResponseEntity eliminarAdministrador
            (@RequestParam String email) throws Exception{
        try {
            if (administradorService.buscarAdministrador(email) != null) {
                int id_admin = administradorService.buscarAdministrador(email).getId_admin();
                administradorService.eliminarAdministrador(email);
                administradorService.disminuirID(id_admin);
                return new ResponseEntity("<h1>Cliente" + email + "eliminado.</h1>", HttpStatus.OK);
            }else{
                return new ResponseEntity("<h1>Usuario no existe.</h1>",HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity("<h1>Cliente imposible de eliminar</h1>",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/administrador/buscar")
    public ResponseEntity buscarAdministrador
            (@RequestParam String email) throws Exception{
        try {
            if (administradorService.buscarAdministrador(email) != null){
                Administrador administrador = administradorService.buscarAdministrador(email);
                return new ResponseEntity(administrador, HttpStatus.OK);
            }else{
                return new ResponseEntity("<h1>Usuario no existe.</h1>", HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity("<h1>Administrador imposible de encontrar.</h1>",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/administrador/mostrar")
    public ResponseEntity mostrarAdministrador() throws Exception {
        List<Administrador> administradores = administradorService.mostrarAdministrador();
        return new ResponseEntity(administradores,HttpStatus.OK);
    }
}
