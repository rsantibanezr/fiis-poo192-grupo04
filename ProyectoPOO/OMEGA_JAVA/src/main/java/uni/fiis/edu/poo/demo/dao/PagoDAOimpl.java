package uni.fiis.edu.poo.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uni.fiis.edu.poo.demo.bean.Pago;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PagoDAOimpl implements PagoDAO {

    @Autowired
    private JdbcTemplate template;

    @Override
    public void registrarPago(Pago pago) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "INSERT INTO PAGO VALUES(?, ?, ?, ?)";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setInt(1, pago.getId_pago());
        pst.setString(2, pago.getCredit_card());
        pst.setByte(3,pago.getCant_persona());
        pst.setString(4,pago.getFecha());
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void actualizarPago( int id_pago, byte cant_persona) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "UPDATE PAGO SET CANT_PERSONA = ? WHERE ID_PAGO = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(1, cant_persona);
        pst.setInt(2, id_pago);
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void eliminarPago(int id_pago) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "DELETE FROM PAGO WHERE ID_PAGO = ?";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setInt(1, id_pago);
        pst.execute();
        pst.close();
        connection.close();
        disminuirID(id_pago);
    }

    @Override
    public Pago buscarPago(int id_pago) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM PAGO WHERE ID_PAGO = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setInt(1, id_pago);
        ResultSet rs = pst.executeQuery();
        Pago pago = null;
        if (rs.next()){
            String credit_card = rs.getString(2);
            byte cant_persona = rs.getByte(3);
            String fecha = rs.getString(4);
            pago = new Pago(id_pago,credit_card,cant_persona,fecha);
        }
        rs.close();
        pst.close();
        connection.close();
        return pago;
    }

    @Override
    public List<Pago> mostrarPago() throws Exception {
        Connection connection = template.getDataSource().getConnection();
        List<Pago> pagos = new ArrayList<>();
        Statement st = connection.createStatement();
        String sql = "SELECT * FROM PAGO;";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()){
            int id_pago = rs.getInt(1);
            String credit_card = rs.getString(2);
            byte cant_persona = rs.getByte(3);
            String fecha = rs.getString(4);
            Pago pago = new Pago(id_pago,credit_card,cant_persona,fecha);
            pagos.add(pago);
        }
        rs.close();
        st.close();
        connection.close();
        return pagos;
    }

    @Override
    public int cantidadPago() throws Exception {
        Connection connection = template.getDataSource().getConnection();
        Statement st = connection.createStatement();
        String sql = "SELECT COUNT (*) FROM PAGO;";
        ResultSet rs = st.executeQuery(sql);
        rs.next();
        int cant = rs.getInt(1);
        rs.close();
        st.close();
        connection.close();
        return cant;
    }

    @Override
    public void disminuirID(int id_pago) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM PAGO ORDER BY ID_PAGO;";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            if(rs.getInt(1)>id_pago){
                String sql2 = "UPDATE PAGO SET ID_PAGO = ID_PAGO - 1" + "\nWHERE ID_PAGO = ? ;";
                PreparedStatement pst = connection.prepareStatement(sql2);
                pst.setInt(1,rs.getInt(1));
                pst.executeUpdate();
                pst.close();
            }
        }
        rs.close();
        st.close();
        connection.close();
    }
}
