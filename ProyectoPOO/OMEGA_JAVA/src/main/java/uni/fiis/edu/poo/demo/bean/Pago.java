package uni.fiis.edu.poo.demo.bean;

public class Pago {
    private int id_pago; //PK
    private String credit_card;
    private byte cant_persona;
    private String fecha;

    public Pago(int id_pago, String credit_card, byte cant_persona, String fecha) {
        this.id_pago = id_pago;
        this.credit_card = credit_card;
        this.cant_persona = cant_persona;
        this.fecha = fecha;
    }

    public int getId_pago() {
        return id_pago;
    }

    public void setId_pago(int id_pago) {
        this.id_pago = id_pago;
    }

    public String getCredit_card() {
        return credit_card;
    }

    public void setCredit_card(String credit_card) {
        this.credit_card = credit_card;
    }

    public byte getCant_persona() {
        return cant_persona;
    }

    public void setCant_persona(byte cant_persona) {
        this.cant_persona = cant_persona;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
