package uni.fiis.edu.poo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uni.fiis.edu.poo.demo.bean.Cliente;
import uni.fiis.edu.poo.demo.dao.ClienteDAO;
import uni.fiis.edu.poo.demo.dao.ClienteDAOimpl;
import uni.fiis.edu.poo.demo.service.ClienteService;

import java.sql.Connection;
import java.util.List;

@RestController
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping("/cliente/registrar")
    public ResponseEntity registrarCliente
            (@RequestParam String cod_uni,
             @RequestParam String password,
             @RequestParam String nombre,
             @RequestParam String ap_paterno,
             @RequestParam String ap_materno,
             @RequestParam String dni,
             @RequestParam byte ciclo,
             @RequestParam String facultad,
             @RequestParam String email,
             @RequestParam int telefono) throws Exception {
        Cliente cliente = new Cliente(cod_uni,password,nombre,ap_paterno,ap_materno,dni,ciclo,facultad,email,telefono);
        try {
            return clienteService.registrarCliente(cliente);

        } catch (Exception e){
            return new ResponseEntity("Cliento no se pudo registrar.",HttpStatus.BAD_GATEWAY);
        }
    }

    @GetMapping("/cliente/actualizar")
    public String actualizarCliente
            (@RequestParam String cod_uni,
             @RequestParam String newpassword) throws Exception{
        try {
            if (clienteService.buscarCliente(cod_uni) != null) {
                clienteService.actualizarCliente(cod_uni, newpassword);
                return "<h1>Usuario: "+ cod_uni +" ha sido actualizado.</h1>";
            }else{
                return "<h1>Usuario no existe.</h1>";
            }
        } catch (Exception e){
            return "<h1>No se puedo actualizar usuario.</h1>";
        }
    }

    @GetMapping("/cliente/eliminar")
    public String eliminarCliente
            (@RequestParam String cod_uni) throws Exception{
        try {
            if (clienteService.buscarCliente(cod_uni) != null) {
                clienteService.eliminarCliente(cod_uni);
                return "<h1>Cliente" + cod_uni + "eliminado.</h1>";
            }else{
                return "<h1>Usuario no existe.</h1>";
            }
        } catch (Exception e){
            return "<h1>Cliente imposible de eliminar</h1>";
        }
    }

    @GetMapping("/cliente/buscar")
    public ResponseEntity buscarCliente
            (@RequestParam String cod_uni) throws Exception{
        try {
            if (clienteService.buscarCliente(cod_uni) != null){
                Cliente cliente = clienteService.buscarCliente(cod_uni);
                return new ResponseEntity(cliente, HttpStatus.OK);
            }else{
                return new ResponseEntity("<h1>Usuario no existe.</h1>", HttpStatus.OK);
            }
        } catch (Exception e){
            return new ResponseEntity("<h1>Cliente imposible de encontrar.</h1>",HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/cliente/mostrar")
    public ResponseEntity mostrarCliente() throws Exception {
        try {
            return new ResponseEntity(clienteService.mostrarCliente(),HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity("No se puede mostrar clientes.",HttpStatus.BAD_GATEWAY);
        }

    }

}
