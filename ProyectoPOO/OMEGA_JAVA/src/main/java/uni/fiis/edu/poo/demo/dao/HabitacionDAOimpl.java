package uni.fiis.edu.poo.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uni.fiis.edu.poo.demo.bean.Habitacion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class HabitacionDAOimpl implements HabitacionDAO {

    @Autowired
    private JdbcTemplate template;

    @Override
    public void registrarHabitacion(Habitacion habitacion) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "INSERT INTO HABITACION VALUES(?, ?);";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(1, habitacion.getId_hab());
        pst.setString(2, habitacion.getDescripcion());
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void actualizarHabitacion(byte id_hab, String descripcion) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "UPDATE HABITACION SET DESCRIPCION = ? WHERE ID_HAB = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(2,id_hab);
        pst.setString(1,descripcion);
        pst.execute();
        System.out.print("Habitacion: "+id_hab +" ha sido actualizado.");
        pst.close();
        connection.close();
    }

    @Override
    public void eliminarHabitacion(byte id_hab) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "DELETE FROM HABITACION WHERE ID_HAB = ?;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(1, id_hab);
        pst.executeUpdate();
        pst.close();
        connection.close();
        disminuirID(id_hab);
    }

    @Override
    public Habitacion buscarHabitacion(byte id_hab) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM HABITACION WHERE ID_HAB = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setByte(1, id_hab);
        ResultSet rs = pst.executeQuery();
        Habitacion habitacion = null;
        if (rs.next()){
            String descripcion = rs.getString(2);
            habitacion = new Habitacion(id_hab,descripcion);
        }
        rs.close();
        pst.close();
        connection.close();

        return habitacion;
    }

    @Override
    public List<Habitacion> mostrarHabitacion() throws Exception {
        Connection connection = template.getDataSource().getConnection();
        List<Habitacion> habitacion = new ArrayList<>();
        Statement st = connection.createStatement();
        String sql = "SELECT * FROM HABITACION ORDER BY ID_HAB;";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()){
            Byte id_hab = rs.getByte(1);
            String descripcion = rs.getString(2);
            Habitacion hab = new Habitacion(id_hab,descripcion);
            habitacion.add(hab);
        }
        rs.close();
        st.close();
        connection.close();
        return habitacion;
    }

    @Override
    public byte cantidadHabitacion() throws Exception{
        Connection connection = template.getDataSource().getConnection();
        Statement st = connection.createStatement();
        String sql = "SELECT COUNT (*) FROM HABITACION;";
        ResultSet rs = st.executeQuery(sql);
        rs.next();
        byte cant = rs.getByte(1);
        rs.close();
        st.close();
        connection.close();
        return cant;
    }

    @Override
    public void disminuirID(byte id_hab) throws Exception{
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM HABITACION ORDER BY ID_HAB;";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            if(rs.getByte(1)>id_hab){
                String sql2 = "UPDATE HABITACION SET ID_HAB = ID_HAB - 1" + "\nWHERE ID_HAB = ? ;";
                PreparedStatement pst = connection.prepareStatement(sql2);
                pst.setByte(1,rs.getByte(1));
                pst.executeUpdate();
                pst.close();
            }
        }
        rs.close();
        st.close();
        connection.close();
    }
}
