package uni.fiis.edu.poo.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uni.fiis.edu.poo.demo.bean.Monto;
import uni.fiis.edu.poo.demo.dao.MontoDAO;

import java.util.List;

@Service
public class MontoService {

    @Autowired
    MontoDAO montoDAO;

    public ResponseEntity registrarMonto(Monto monto) throws Exception{
        if (monto.getCant_persona()<7){
            montoDAO.registrarMonto(monto);
            return new ResponseEntity("Monto registrado.", HttpStatus.OK);
        }else {
            return new ResponseEntity("Monto no se pudo registrar", HttpStatus.OK);
        }
    }

    public void actualizarMonto(byte cant_persona, double monto) throws Exception{
        montoDAO.actualizarMonto(cant_persona, monto);
    }

    public void eliminarMonto(byte cant_persona) throws Exception{
        montoDAO.eliminarMonto(cant_persona);
    }

    public Monto buscarMonto(byte cant_persona) throws Exception{
        return montoDAO.buscarMonto(cant_persona);
    }

    public List<Monto> mostrarMonto() throws Exception{
        return montoDAO.mostrarMonto();
    }
}
