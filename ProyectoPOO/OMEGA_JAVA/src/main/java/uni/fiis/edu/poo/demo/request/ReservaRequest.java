package uni.fiis.edu.poo.demo.request;

public class ReservaRequest {
    private String cod_uni;
    private int id_pago;
    private byte id_hab;
    private String fecha;

    public String getCod_uni() {
        return cod_uni;
    }

    public void setCod_uni(String cod_uni) {
        this.cod_uni = cod_uni;
    }

    public int getId_pago() {
        return id_pago;
    }

    public void setId_pago(int id_pago) {
        this.id_pago = id_pago;
    }

    public byte getId_hab() {
        return id_hab;
    }

    public void setId_hab(byte id_hab) {
        this.id_hab = id_hab;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
