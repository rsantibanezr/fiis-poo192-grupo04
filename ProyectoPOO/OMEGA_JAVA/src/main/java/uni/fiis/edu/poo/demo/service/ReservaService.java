package uni.fiis.edu.poo.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uni.fiis.edu.poo.demo.bean.Reserva;
import uni.fiis.edu.poo.demo.dao.ReservaDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

@Service
public class ReservaService {

    @Autowired
    ReservaDAO reservaDAO;

    @Autowired
    JdbcTemplate template;

    public ResponseEntity registrarReserva(Reserva reserva) throws Exception{
        Connection connection = template.getDataSource().getConnection();
        String sql1 = "SELECT COUNT(*) FROM RESERVA WHERE ID_PAGO = ?;";
        PreparedStatement pst1 = connection.prepareStatement(sql1);
        pst1.setInt(1,reserva.getId_pago());
        ResultSet rs = pst1.executeQuery();
        rs.next();
        int cant = rs.getInt(1);
        rs.close();
        pst1.close();
        connection.close();
        if (cant==0){
            reservaDAO.registrarReserva(reserva);
            return new ResponseEntity("Reserva registrada.", HttpStatus.OK);
        }else {
            return new ResponseEntity("Reserva ya registrada",HttpStatus.OK);
        }
    }

    public void actualizarReserva(String cod_uni, String fecha) throws Exception{
        reservaDAO.actualizarReserva(cod_uni, fecha);
    }

    public void eliminarReserva(String cod_uni, String fecha) throws Exception{
        reservaDAO.eliminarReserva(cod_uni, fecha);
    }

    public Reserva buscarReserva(String cod_uni) throws Exception{
        return reservaDAO.buscarReserva(cod_uni);
    }

    public List<Reserva> mostrarReserva() throws Exception{
        return reservaDAO.mostrarReserva();
    }
}
