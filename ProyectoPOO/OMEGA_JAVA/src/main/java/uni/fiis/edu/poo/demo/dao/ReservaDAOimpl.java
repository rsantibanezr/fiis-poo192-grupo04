package uni.fiis.edu.poo.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import uni.fiis.edu.poo.demo.bean.Reserva;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ReservaDAOimpl implements ReservaDAO {

    @Autowired
    private JdbcTemplate template;

    @Override
    public void registrarReserva(Reserva reserva) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "INSERT INTO RESERVA VALUES(?, ?, ?, ?)";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, reserva.getCod_uni());
        pst.setInt(2, reserva.getId_pago());
        pst.setByte(3, reserva.getId_hab());
        pst.setString(4, reserva.getFecha());
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void actualizarReserva(String cod_uni, String fecha) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "UPDATE RESERVA SET FECHA = ? WHERE COD_UNI = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, fecha);
        pst.setString(2, cod_uni);
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public void eliminarReserva(String cod_uni, String fecha) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "DELETE FROM RESERVA WHERE (COD_UNI = ?) AND (FECHA=?);";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, cod_uni);
        pst.setString(2,fecha);
        pst.execute();
        pst.close();
        connection.close();
    }

    @Override
    public Reserva buscarReserva(String cod_uni) throws Exception {
        Connection connection = template.getDataSource().getConnection();
        String sql = "SELECT * FROM RESERVA WHERE COD_UNI = ? ;";
        PreparedStatement pst = connection.prepareStatement(sql);
        pst.setString(1, cod_uni);
        ResultSet rs = pst.executeQuery();
        Reserva reserva = null;
        if (rs.next()){
            int id_pago = rs.getInt(2);
            byte id_hab = rs.getByte(3);
            String fecha = rs.getString(4);
            reserva = new Reserva(cod_uni,id_pago,id_hab,fecha);
        }
        rs.close();
        pst.close();
        connection.close();
        return reserva;
    }

    @Override
    public List<Reserva> mostrarReserva() throws Exception {
        Connection connection = template.getDataSource().getConnection();
        List<Reserva> reservas = new ArrayList<>();
        Statement st = connection.createStatement();
        String sql = "SELECT * FROM RESERVA ORDER BY FECHA;";
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()){
            String cod_uni = rs.getString(1);
            int id_pago = rs.getInt(2);
            byte id_hab = rs.getByte(3);
            String fecha = rs.getString(4);
            Reserva reserva = new Reserva(cod_uni,id_pago,id_hab,fecha);
            reservas.add(reserva);
        }
        rs.close();
        st.close();
        connection.close();
        return reservas;
    }

}
