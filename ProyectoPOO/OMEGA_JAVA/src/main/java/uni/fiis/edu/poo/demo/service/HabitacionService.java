package uni.fiis.edu.poo.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uni.fiis.edu.poo.demo.bean.Habitacion;
import uni.fiis.edu.poo.demo.dao.HabitacionDAO;

import java.util.List;

@Service
public class HabitacionService {

    @Autowired
    HabitacionDAO habitacionDAO;

    public void registrarHabitacion(Habitacion habitacion) throws Exception{
        habitacionDAO.registrarHabitacion(habitacion);
    }

    public void actualizarHabitacion(byte id_hab, String descripcion) throws Exception{
        habitacionDAO.actualizarHabitacion(id_hab, descripcion);
    }

     public void eliminarHabitacion(byte id_hab) throws Exception{
        habitacionDAO.eliminarHabitacion(id_hab);
     }

     public Habitacion buscarHabitacion(byte id_hab) throws Exception{
        return habitacionDAO.buscarHabitacion(id_hab);
     }

     public List<Habitacion> mostrarHabitacion() throws Exception{
        return habitacionDAO.mostrarHabitacion();
     }

     public byte cantidadHabitacion() throws Exception{
        return habitacionDAO.cantidadHabitacion();
     }

     public void disminuirID(byte id_hab) throws Exception{
        habitacionDAO.disminuirID(id_hab);
     }
}
