package uni.fiis.edu.poo.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uni.fiis.edu.poo.demo.bean.Pago;
import uni.fiis.edu.poo.demo.dao.PagoDAO;

import java.util.List;

@Service
public class PagoService {

    @Autowired
    PagoDAO pagoDAO;

    public void registrarPago(Pago pago) throws Exception{
        pagoDAO.registrarPago(pago);
    }

    public void actualizarPago(int id_pago, byte cant_persona) throws Exception{
        pagoDAO.actualizarPago(id_pago, cant_persona);
    }

    public void eliminarPago(int id_pago) throws Exception{
        pagoDAO.eliminarPago(id_pago);
    }

    public Pago buscarPago(int id_pago) throws Exception{
        return pagoDAO.buscarPago(id_pago);
    }

    public List<Pago> mostrarPago() throws Exception{
        return pagoDAO.mostrarPago();
    }

    public int cantidadPago() throws Exception{
        return pagoDAO.cantidadPago();
    }

    public void disminuirID(int id_pago) throws Exception{
        pagoDAO.disminuirID(id_pago);
    }
}
